cd ..
mvn package spring-boot:repackage -DskipTests -Pint
cp wichteli-provider/target/wichteli-provider-0.0.1-SNAPSHOT.jar docker/files/wichteli-provider.jar
cd docker || exit 1

docker build . --tag=novarx/wichteli
