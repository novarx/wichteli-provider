#!/bin/sh

# Wait for MySql Host to be up
until nc -zv $MYSQL_HOST 3306; do
  echo >&2 "MySql is unavailable - sleeping"
  sleep 1
done
echo >&2 "MySql is up - executing command"

# Start Server
exec java -jar /opt/wichteli/lib/wichteli-provider.jar
