package ch.novarx.wichteli.entity;

import org.junit.jupiter.api.Test;

import static org.junit.Assert.assertEquals;

public class UserEntityTest {

    @Test
    public void should_get_and_set_properties_correct() {
        UserEntity userEntity = new UserEntity();
        userEntity.setUsername("hans");
        userEntity.setPassword("secret !!11");
        userEntity.setEmail("info@test.test");

        assertEquals(0, userEntity.getId());
        assertEquals("hans", userEntity.getUsername());
        assertEquals("secret !!11", userEntity.getPassword());
        assertEquals("info@test.test", userEntity.getEmail());
    }

}