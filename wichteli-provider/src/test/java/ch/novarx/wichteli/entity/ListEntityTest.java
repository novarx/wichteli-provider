package ch.novarx.wichteli.entity;

import ch.novarx.wichteli.types.ListStatusEnum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class ListEntityTest {

    private ListEntity aList;

    @BeforeEach
    void setup() {
        UserEntity admin = UserEntity.builder().id(555).build();
        aList = new ListEntity();
        aList.setAdminId(admin.getId());
        aList.setName("My List");
        aList.addUser(admin);
    }

    @Test
    public void should_initialize_attributes_correct() {
        assertEquals("My List", aList.getName());
        assertEquals(555, aList.getAdminId());
        assertEquals(ListStatusEnum.INVITING, aList.getStatus());
        assertEquals(1, aList.getUsers().size());
    }

    @Test
    public void should_get_and_set_properties_correct() {
        aList.setStatus(ListStatusEnum.PICKING);
        aList.setJoinCode("LoremCode");
        aList.setId(22);
        aList.setUsers(aList.getUsers());

        assertEquals(22, aList.getId());
        assertEquals("My List", aList.getName());
        assertEquals(555, aList.getAdminId());
        assertEquals(ListStatusEnum.PICKING, aList.getStatus());
        assertEquals(0, aList.getImps().size());
    }

    @Test
    public void should_only_set_valid_status_values() {
        aList.setStatus(ListStatusEnum.PICKING);
        assertEquals(ListStatusEnum.PICKING, aList.getStatus());

        aList.setStatus(9999);
        assertEquals(ListStatusEnum.PICKING, aList.getStatus());
    }

    @Test
    public void should_add_and_get_users_correct() {
        assertEquals(1, aList.getUsers().size());
        aList.addUser(aUser(666));
        aList.addUser(aUser(777));
        assertEquals(3, aList.getUsers().size());
    }

    @Test
    public void should_add_a_map_of_users_correct() {
        // when admin is in Map to add
        Map<Long, UserEntity> users = new HashMap<>();
        users.put(555L, aUser(555)); // to test specificly, that adding admin does not throw an exception
        users.put(666L, aUser(666));
        users.put(777L, aUser(777));
        aList.addUsers(users);

        assertEquals(3, aList.getUsers().size());
    }

    @Test
    public void should_remove_user_correct() {
        // given
        aList.addUser(aUser(666));
        aList.addUser(aUser(777));

        // when
        aList.removeUser(666);

        // then
        assertEquals(2, aList.getUsers().size());
    }

    @Test
    public void should_remove_imp_correct() {
        // given
        aList.addImp(aImp(666, 99));
        aList.addImp(aImp(777, 98));

        // when
        aList.removeImp(666);

        // then
        assertEquals(1, aList.getImps().size());
    }

    @Test
    public void should_add_imp_correct() {
        aList.addImp(aImp(666, 99));
        aList.addImp(aImp(666, 99));

        ImpEntity imp = aImp(777, 98);
        imp.setReciver(aUser(90));
        aList.addImp(imp);
        aList.addImp(imp);

        ImpEntity anotherImp = aImp(666, 99);
        anotherImp.setMayNot(true);
        aList.addImp(anotherImp);
        aList.addImp(anotherImp);

        assertEquals(3, aList.getImps().size());
    }

    @Test
    public void should_add_same_user_only_once() {
        // when
        aList.addUser(aUser(666));
        aList.addUser(aUser(666));

        // then
        assertEquals(2, aList.getUsers().size());
    }

    @Test
    public void should_not_remove_admin_from_users_map() {
        aList.removeUser(555);
        assertEquals(1, aList.getUsers().size());
        assertEquals(555, aList.getUsers().get(555L).getId());
    }

    @Test
    public void should_get_may_not() {
        ImpEntity imp = aImp(22, 22);
        imp.setMayNot(true);
        aList.addImp(imp);
        assertEquals(1, aList.getMayNot().size());

        imp = aImp(22, 33);
        imp.setMayNot(true);
        aList.addImp(imp);
        assertEquals(2, aList.getMayNot().size());

        imp = aImp(22, 44);
        imp.setMayNot(false);
        aList.addImp(imp);
        assertEquals(2, aList.getMayNot().size());
    }

    private UserEntity aUser(long id) {
        return UserEntity.builder().id(id).build();
    }

    private ImpEntity aImp(long id, long giverId) {
        return ImpEntity.builder()
                .id(id)
                .giver(aUser(giverId))
                .reciver(aUser(giverId + 1))
                .build();
    }

}