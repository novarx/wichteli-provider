package ch.novarx.wichteli.security;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.util.ReflectionTestUtils;

import java.util.Date;

import static java.util.Collections.emptyList;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JwtTokenUtilTest {

  @Mock
  private TimeProvider timeProvider;

  @InjectMocks
  private JwtTokenUtil sut = new JwtTokenUtil();

  @BeforeEach
  void setup() {
    ReflectionTestUtils.setField(sut, "secret", "LoremIpsum");
  }

  @AfterEach
  void teardown() {
    verifyNoMoreInteractions(timeProvider);
  }

  @Test
  public void should_get_a_token() {
    UserDetails user = new User("myname", "password", emptyList());
    when(timeProvider.currentTimeMillis()).thenReturn(System.currentTimeMillis());

    String token = sut.generateToken(user);

    assertNotNull(token);
    assertEquals("myname", sut.getUsernameFromToken(token));
    assertNotNull(sut.getExpirationDateFromToken(token));
    assertTrue(new Date().before(sut.getExpirationDateFromToken(token)));
    assertTrue(sut.validateToken(token, user));

    verify(timeProvider, times(2)).currentTimeMillis();
  }

  @Test
  public void should_validate_a_expired_token() {
    UserDetails user = new User("myname", "password", emptyList());
    when(timeProvider.currentTimeMillis()).thenReturn(1590000000000L);

    String token = sut.generateToken(user);

    assertFalse(sut.validateToken(token, user));
    verify(timeProvider, times(2)).currentTimeMillis();
  }

  @Test
  public void should_validate_a_token_with_wrong_user() {
    UserDetails user = new User("myname", "password", emptyList());
    UserDetails wrongUser = new User("myothername", "password", emptyList());
    when(timeProvider.currentTimeMillis()).thenReturn(System.currentTimeMillis());

    String token = sut.generateToken(user);

    assertFalse(sut.validateToken(token, wrongUser));
    verify(timeProvider, times(2)).currentTimeMillis();
  }

}
