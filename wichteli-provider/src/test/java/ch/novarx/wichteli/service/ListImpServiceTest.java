package ch.novarx.wichteli.service;

import ch.novarx.wichteli.entity.*;
import ch.novarx.wichteli.service.imp.ListImpService;
import ch.novarx.wichteli.service.imp.RandomNumberService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.*;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toMap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
class ListImpServiceTest {

  @Mock
  private ImpRepository impRepository;

  @Mock
  private ListRepository listRepository;

  @Mock
  private RandomNumberService randomNumberService;

  @InjectMocks
  private ListImpService sut;
  private long listId = 55L;

  @AfterEach
  void teardown() {
    verifyNoMoreInteractions(impRepository, listRepository);
  }

  @Test
  public void make_sure_everything_is_wired_up_correctly() {
    assertNotNull(sut);
  }

  @Test
  public void should_pick_imps_when_two_users() {
    ListEntity list = aList(2);
    List<Long> userIds = getIds(list);

    when(listRepository.findById(listId)).thenReturn(Optional.of(list));

    sut.pickImps(listId);

    assertListContainsImps(list, userIds);

    verify(listRepository).findById(listId);

    assertImpsSaved(userIds);
    verify(listRepository).save(list);
  }

  @Test
  public void should_pick_imps_when_more_users() {
    List<Integer> listSizes = List.of(3, 4, 5, 6, 7, 8, 9, 10, 20, 30, 40);
    listSizes.forEach(i -> {
      impPicking(i);
      Mockito.reset(impRepository, listRepository);
    });
  }

  @Test
  public void should_pick_imps_with_respecting_mayNot() {
    ListEntity list = aList(9);
    List<Long> userIds = getIds(list);

    List<ImpEntity> mayNots = MayNotBuilder.builder(list)
      .add(1, 2)
      .add(2, 1)
      .add(3, 4)
      .build();
    list.setImps(mayNots);

    when(listRepository.findById(listId)).thenReturn(Optional.of(list));

    sut.pickImps(listId);

    verify(listRepository).findById(listId);
    assertListContainsImps(list, userIds);
    assertImpsSaved(userIds);
    assertNoMayNotSavedFor(list);
    verify(listRepository).save(list);
  }

  @Test
  public void should_pick_randomly() {
    when(randomNumberService.randomIntTo(anyInt())).thenAnswer(args -> 0);
    Map<Long, ImpEntity> savedImps1 = impPicking(3).stream()
      .collect(toMap(i -> i.getReciver().getId(), i -> i));
    verify(randomNumberService, times(3)).randomIntTo(anyInt());
    assertEquals(1, savedImps1.get(0L).getGiver().getId());
    assertEquals(2, savedImps1.get(1L).getGiver().getId());
    assertEquals(0, savedImps1.get(2L).getGiver().getId());

    Mockito.reset(randomNumberService, impRepository, listRepository);

    when(randomNumberService.randomIntTo(anyInt())).thenAnswer(args -> args.getArgument(0));
    Map<Long, ImpEntity> savedImps2 = impPicking(3).stream()
      .collect(toMap(i -> i.getReciver().getId(), i -> i));
    verify(randomNumberService, times(3)).randomIntTo(anyInt());
    assertEquals(2, savedImps2.get(0L).getGiver().getId());
    assertEquals(0, savedImps2.get(1L).getGiver().getId());
    assertEquals(1, savedImps2.get(2L).getGiver().getId());
  }

  @Test
  public void should_throw_when_not_enough_imps_for_everybody() {
    ListEntity list = aList(3);

    List<ImpEntity> mayNots = MayNotBuilder.builder(list)
      .add(1, 2)
      .add(2, 1)
      .build();
    list.setImps(mayNots);

    when(listRepository.findById(listId)).thenReturn(Optional.of(list));

    Exception exception = assertThrows(RuntimeException.class, () -> sut.pickImps(listId));

    assertEquals("Not enough imps for Everybody!", exception.getMessage());
    verify(listRepository).findById(listId);
  }

  @Test
  public void should_throw_when_only_one_imp_in_list() {
    ListEntity list = aList(1);
    when(listRepository.findById(listId)).thenReturn(Optional.of(list));

    Exception exception = assertThrows(RuntimeException.class, () -> sut.pickImps(listId));

    assertEquals("Not enough imps for Everybody!", exception.getMessage());
    verify(listRepository).findById(listId);
  }

  private void assertListContainsImps(ListEntity list, List<Long> userIds) {
    assertThat(list.getImps().stream().filter(i -> !i.isMayNot()).count()).isEqualTo(userIds.size());
    assertThat(list.getImps().stream().map(i -> i.getReciver().getId())).containsAll(userIds);
    assertThat(list.getImps().stream().map(i -> i.getGiver().getId())).containsAll(userIds);
  }

  private void assertImpsSaved(List<Long> userIds) {
    List<ImpEntity> imps = getSavedImpEntities(userIds);

    List<Long> capturedGiverIds = imps.stream().map(i -> i.getGiver().getId()).collect(toList());
    List<Long> capturedReciverIds = imps.stream().map(i -> i.getReciver().getId()).collect(toList());

    imps.forEach(imp -> assertNotEquals(imp.getGiver().getId(), imp.getReciver().getId()));

    userIds.forEach(id -> assertThat(capturedGiverIds).containsOnlyOnce(id));
    assertEquals(userIds.size(), capturedGiverIds.size());
    userIds.forEach(id -> assertThat(capturedReciverIds).containsOnlyOnce(id));
    assertEquals(userIds.size(), capturedReciverIds.size());
  }

  private void assertNoMayNotSavedFor(ListEntity list) {
    List<Long> userIds = getIds(list);

    getSavedImpEntities(userIds).forEach(savedImp -> {
      list.getMayNot().forEach(mayNot -> {
        assertThat(List.of(savedImp.getGiver().getId(), savedImp.getReciver().getId()))
          .doesNotContainSequence(List.of(mayNot.getGiver().getId(), mayNot.getReciver().getId()));
      });
    });
  }

  private List<ImpEntity> impPicking(int listSize) {
    ListEntity list = aList(listSize);
    List<Long> userIds = getIds(list);

    when(listRepository.findById(listId)).thenReturn(Optional.of(list));

    sut.pickImps(listId);

    assertListContainsImps(list, userIds);
    verify(listRepository).findById(listId);
    assertImpsSaved(userIds);
    verify(listRepository).save(list);
    return getSavedImpEntities(userIds);
  }

  private List<ImpEntity> getSavedImpEntities(List<Long> userIds) {
    ArgumentCaptor<ImpEntity> impArgument = ArgumentCaptor.forClass(ImpEntity.class);
    verify(impRepository, times(userIds.size())).save(impArgument.capture());
    return impArgument.getAllValues();
  }

  private List<Long> getIds(ListEntity list) {
    return list.getUsers().values().stream().map(UserEntity::getId).collect(toList());
  }

  private ListEntity aList(int size) {
    Map<Long, UserEntity> users = new HashMap<>();
    for (int i = 0; i < size; i++) {
      users.put(((long) i), aUser(i));
    }
    return ListEntity.builder().users(users).build();
  }

  private UserEntity aUser(int i) {
    return UserEntity.builder().id(i).build();
  }

  private static class MayNotBuilder {
    private final ListEntity list;
    private final Map<Integer, Integer> nots = new HashMap<>();

    private MayNotBuilder(ListEntity list) {
      this.list = list;
    }

    public static MayNotBuilder builder(ListEntity list) {
      return new MayNotBuilder(list);
    }

    public MayNotBuilder add(int giver, int reciver) {
      nots.put(giver, reciver);
      return this;
    }

    public List<ImpEntity> build() {
      List<ImpEntity> mayNots = new ArrayList<>();
      nots.forEach((giver, reciver) -> mayNots.add(mayNotOfUserIndex(giver, reciver)));
      return mayNots;
    }

    private ImpEntity mayNotOfUserIndex(int giver, int reciver) {
      return ImpEntity.builder()
        .mayNot(true)
        .giver(userOfIndex(giver))
        .reciver(userOfIndex(reciver))
        .build();
    }

    private UserEntity userOfIndex(int index) {
      return list.getUsers().get((long) index);
    }
  }

  private static class Helpers {

  }

}
