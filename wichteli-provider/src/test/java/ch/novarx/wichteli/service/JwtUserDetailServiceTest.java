package ch.novarx.wichteli.service;

import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.entity.UserRepository;
import ch.novarx.wichteli.security.JwtUserDetailService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class JwtUserDetailServiceTest {

  @Mock
  UserRepository userRepository;

  @Mock
  PasswordEncoder bcryptEncoder;

  @InjectMocks
  JwtUserDetailService sut;

  @BeforeEach
  void setup() {
    verifyNoMoreInteractions(userRepository, bcryptEncoder);
  }

  @Test
  void should_save_a_new_user() {
    when(bcryptEncoder.encode(anyString())).thenReturn("aHashedPassword");
    UserDto user = UserDto.builder()
            .username("lorem")
            .password("aPassword")
            .email("info@example.com")
            .build();
    sut.save(user);

    verify(bcryptEncoder).encode("aPassword");
    verify(userRepository).save(ArgumentMatchers.argThat(v ->
            v.getUsername().equals("lorem") &&
                    v.getPassword().equals("aHashedPassword") &&
                    v.getEmail().equals("info@example.com")
    ));
  }

  @Test
  void should_not_save_existing_user() {
    when(userRepository.findByEmail("lorem")).thenReturn(new UserEntity());

    UserDto user = UserDto.builder().username("lorem").password("aPassword").build();
    assertThrows(Exception.class, () -> sut.save(user));

    verify(userRepository).findByEmail("lorem");
  }

  @Test
  public void should_get_authenticated_user() {
    String username = "lorem";
    Authentication authentication = mockAuthUser(username);
    UserEntity userEntity = UserEntity.builder().id(55).username(username).build();
    when(userRepository.findByEmail(username)).thenReturn(userEntity);

    UserEntity authenticatedUser = sut.authenticatedUser().orElseThrow();

    assertNotNull(authenticatedUser);
    verify(authentication).getName();
    verify(userRepository).findByEmail(anyString());
  }

  @Test
  public void should_get_empty_optional_when_not_autenticated() {
    mockNoUser();
    assertFalse(sut.authenticatedUser().isPresent());
  }

  @Test
  public void should_loadUserByUsername() {
    String email = "lorem@example.org";

    UserEntity user = UserEntity.builder().email(email).password("pass").build();
    when(userRepository.findByEmail(email)).thenReturn(user);

    UserDetails userDetails = sut.loadUserByUsername(email);
    assertEquals(user.getEmail(), userDetails.getUsername());
    assertEquals(user.getPassword(), userDetails.getPassword());

    verify(userRepository).findByEmail(email);
  }

  @Test
  public void should_throw_when_no_user_found_in_loadUserByUsername() {
    String email = "lorem@example.org";

    when(userRepository.findByEmail(email)).thenReturn(null);

    Exception exception = assertThrows(UsernameNotFoundException.class, () -> sut.loadUserByUsername(email));
    assertEquals("User not found with email: lorem@example.org", exception.getMessage());

    verify(userRepository).findByEmail(email);
  }

  private Authentication mockAuthUser(String username) {
    Authentication authentication = Mockito.mock(Authentication.class);
    when(authentication.getName()).thenReturn(username);

    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Mockito.when(securityContext.getAuthentication()).thenReturn(authentication);
    SecurityContextHolder.setContext(securityContext);
    return authentication;
  }

  private SecurityContext mockNoUser() {
    SecurityContext securityContext = Mockito.mock(SecurityContext.class);
    Mockito.when(securityContext.getAuthentication()).thenReturn(null);
    SecurityContextHolder.setContext(securityContext);
    return securityContext;
  }
}
