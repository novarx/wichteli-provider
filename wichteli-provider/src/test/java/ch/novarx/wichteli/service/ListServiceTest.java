package ch.novarx.wichteli.service;

import ch.novarx.wichteli.controller.list.dto.ListDto;
import ch.novarx.wichteli.controller.list.dto.ListUpdateDto;
import ch.novarx.wichteli.entity.*;
import ch.novarx.wichteli.security.JwtUserDetailService;
import ch.novarx.wichteli.service.imp.ListImpService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.access.AccessDeniedException;

import java.util.*;

import static ch.novarx.wichteli.types.ListStatusEnum.*;
import static java.util.Collections.singletonList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ListServiceTest {

  @Mock
  private ListRepository listRepository;

  @Mock
  private UserRepository userRepository;

  @Mock
  private JwtUserDetailService userDetailService;

  @Mock
  private ImpRepository impRepository;

  @Mock
  private ListImpService listImpService;

  @InjectMocks
  private ListService sut;

  @AfterEach
  void teardown() {
    verifyNoMoreInteractions(listRepository, userDetailService, userRepository, impRepository, listImpService);
  }

  @Test
  public void make_sure_everything_is_wired_up_correctly() {
    assertNotNull(sut);
  }

  @Test
  public void should_create_a_fresh_list() {
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(88).build()));
    when(listRepository.save(any(ListEntity.class))).thenAnswer(p -> p.getArgument(0));

    ListDto savedEntity = sut.freshList("A Name");

    ArgumentCaptor<ListEntity> entityToSave = ArgumentCaptor.forClass(ListEntity.class);
    verify(listRepository).save(entityToSave.capture());

    assertEquals(25, savedEntity.getJoinCode().length());
    assertEquals(88L, savedEntity.getAdminId());
    assertEquals("A Name", savedEntity.getName());
    assertEquals(INVITING, savedEntity.getStatus());
    assertEquals(88, savedEntity.getUsers().get(88L).getId());
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_join_user_to_existing_list() {
    ListEntity entity = new ListEntity();
    String joinCode = "loremIpsumDolor";

    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(55).build()));
    when(listRepository.findByJoinCode(joinCode)).thenReturn(Optional.of(entity));
    when(listRepository.save(entity)).thenReturn(entity);

    ListDto entityWithJoined = sut.join(joinCode);

    assertEquals(55L, entity.getUsers().get(55L).getId());
    verify(listRepository).findByJoinCode(joinCode);
    verify(listRepository).save(entity);
  }

  private UserEntity aUser(long id) {
    return UserEntity.builder().id(id).build();
  }

  @Test
  public void should_handle_join_when_already_in_list() {
    ListEntity entity = new ListEntity();
    entity.addUser(aUser(66));
    entity.addUser(aUser(55));
    String joinCode = "loremIpsumDolor";

    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(55).build()));
    when(listRepository.findByJoinCode(joinCode)).thenReturn(Optional.of(entity));
    when(listRepository.save(entity)).thenReturn(entity);

    sut.join(joinCode);

    assertEquals(55, entity.getUsers().get(55L).getId());
    assertEquals(66, entity.getUsers().get(66L).getId());
    verify(listRepository).findByJoinCode(joinCode);
    verify(listRepository).save(entity);
  }

  @Test
  public void should_throw_when_no_list_list_found_with_joinCode() {
    String joinCode = "xxxxxxxxx";
    when(listRepository.findByJoinCode(anyString())).thenReturn(Optional.empty());

    Exception exception = assertThrows(Exception.class, () -> sut.join(joinCode));

    assertEquals("joinCode: " + joinCode + " not found", exception.getMessage());
    verify(listRepository).findByJoinCode(anyString());
  }

  @Test
  public void should_remove_user_from_list() {
    UserEntity authenticatedUser = UserEntity.builder().id(88L).build();
    ListEntity listEntity = ListEntity.builder()
      .id(1)
      .adminId(88)
      .users(new HashMap<>(Map.of(55L, aUser(55), 88L, aUser(88))))
      .build();

    when(listRepository.findById(1L)).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    ListDto list = sut.leave(1L, 55L);

    assertEquals(listEntity.getId(), list.getId());
    assertFalse(list.getUsers().containsKey(55L));

    verify(userDetailService, times(2)).authenticatedUser();
    verify(listRepository).save(listEntity);
    verify(listRepository).findById(1L);
  }

  @Test
  public void should_remove_authenticated_from_list() {
    UserEntity authenticatedUser = UserEntity.builder().id(55L).build();
    ListEntity listEntity = ListEntity.builder()
      .id(1)
      .adminId(88)
      .users(new HashMap<>(Map.of(55L, aUser(55), 88L, aUser(88))))
      .build();

    when(listRepository.findById(1L)).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    ListDto list = sut.leave(1L, 55L);

    assertEquals(listEntity.getId(), list.getId());
    assertFalse(list.getUsers().containsKey(55L));

    verify(userDetailService, times(2)).authenticatedUser();
    verify(listRepository).save(listEntity);
    verify(listRepository).findById(1L);
  }

  @Test
  public void should_not_remove_user_when_not_self_not_admin_from_list() {
    UserEntity authenticatedUser = UserEntity.builder().id(66L).build();
    ListEntity listEntity = ListEntity.builder()
      .id(1)
      .adminId(88)
      .users(new HashMap<>(Map.of(55L, aUser(55), 88L, aUser(88))))
      .build();

    when(listRepository.findById(1L)).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));

    Exception exception = assertThrows(AccessDeniedException.class, () -> sut.leave(1L, 55L));

    assertEquals("Unauthorized", exception.getMessage());
    verify(userDetailService).authenticatedUser();
    verify(listRepository).findById(1L);
  }

  @Test
  public void should_get_a_list_the_user_is_admin_of() {
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(1).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(1)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55)
      .adminId(1)
      .status(INVITING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(1L, aUser(1)))
      .build();

    when(listRepository.findById(55L)).thenReturn(Optional.of(loadedEntity));

    ListDto list = sut.getList(55);

    assertEquals(loadedEntity.getId(), list.getId());
    assertThat(list.getImps()).isEmpty();

    verify(listRepository).findById(55L);
    verify(userDetailService, times(2)).authenticatedUser();
  }

  @Test
  public void should_get_a_list_the_user_is_part_of() {
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(1).build()));

    ListEntity loadedEntity = ListEntity.builder()
      .id(55)
      .adminId(2)
      .users(Map.of(1L, aUser(1), 2L, aUser(2)))
      .build();
    when(listRepository.findById(55L)).thenReturn(Optional.of(loadedEntity));

    ListDto list = sut.getList(55);
    assertEquals(loadedEntity.getId(), list.getId());

    verify(listRepository).findById(55L);
    verify(userDetailService, times(2)).authenticatedUser();
  }

  @Test
  public void should_not_get_a_list_the_user_is_not_part_of() {
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(88).build()));

    ListEntity loadedEntity = ListEntity.builder()
      .id(55)
      .adminId(1)
      .users(Map.of(1L, aUser(1)))
      .build();
    when(listRepository.findById(55L)).thenReturn(Optional.of(loadedEntity));

    Exception exception = assertThrows(Exception.class, () -> sut.getList(55));
    assertEquals("Unauthorized", exception.getMessage());

    verify(listRepository).findById(55L);
    verify(userDetailService, times(2)).authenticatedUser();
  }

  @Test
  public void should_get_a_list_with_own_imp_when_status_PICKING() {
    long authId = 1;
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(authId).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(authId)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55L)
      .adminId(authId)
      .status(PICKING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(authId, aUser(authId), 2L, aUser(2L)))
      .build();

    when(listRepository.findById(55L)).thenReturn(Optional.of(loadedEntity));

    ListDto list = sut.getList(55L);

    assertThat(list.getImps()).hasSize(1);
    assertThat(list.getImps()).containsOnlyOnce(imp1);

    verify(listRepository).findById(55L);
    verify(userDetailService, times(2)).authenticatedUser();
  }

  @Test
  public void should_get_a_list_with_own_imp_when_status_RESOLVING() {
    long authId = 1;
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(authId).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(authId)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55L)
      .adminId(authId)
      .status(RESOLVING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(authId, aUser(authId), 2L, aUser(2L)))
      .build();

    when(listRepository.findById(55L)).thenReturn(Optional.of(loadedEntity));

    ListDto list = sut.getList(55L);

    assertThat(list.getImps()).hasSize(2);
    assertThat(list.getImps()).containsOnlyOnce(imp1);
    assertThat(list.getImps()).containsOnlyOnce(imp2);

    verify(listRepository).findById(55L);
    verify(userDetailService, times(2)).authenticatedUser();
  }

  @Test
  public void should_get_all_lists_a_user_is_admin_of() {
    long authId = 1;
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(1).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(authId)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55L)
      .adminId(authId)
      .status(INVITING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(authId, aUser(authId), 2L, aUser(2L)))
      .build();

    when(listRepository.getAllByAdminIdOrUsersIn(1, singletonList(1L))).thenReturn(List.of(loadedEntity));

    List<ListDto> lists = sut.getLists();
    assertEquals(loadedEntity.getId(), lists.get(0).getId());
    assertThat(lists.get(0).getImps()).isEmpty();

    verify(listRepository).getAllByAdminIdOrUsersIn(1, singletonList(1L));
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_get_all_lists_a_user_is_admin_of_when_status_PICKING() {
    long authId = 1;
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(1).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(authId)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55L)
      .adminId(authId)
      .status(PICKING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(authId, aUser(authId), 2L, aUser(2L)))
      .build();

    when(listRepository.getAllByAdminIdOrUsersIn(1, singletonList(1L))).thenReturn(List.of(loadedEntity));

    List<ListDto> lists = sut.getLists();
    assertEquals(loadedEntity.getId(), lists.get(0).getId());
    assertThat(lists.get(0).getImps()).containsOnly(imp1);

    verify(listRepository).getAllByAdminIdOrUsersIn(1, singletonList(1L));
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_get_all_lists_a_user_is_admin_of_when_status_RESOLVING() {
    long authId = 1;
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(1).build()));

    ImpEntity mayNot = ImpEntity.builder().giver(aUser(1)).mayNot(true).build();
    ImpEntity imp1 = ImpEntity.builder().giver(aUser(authId)).mayNot(false).build();
    ImpEntity imp2 = ImpEntity.builder().giver(aUser(2)).mayNot(false).build();

    ListEntity loadedEntity = ListEntity.builder()
      .id(55L)
      .adminId(authId)
      .status(RESOLVING)
      .imps(List.of(mayNot, imp1, imp2))
      .users(Map.of(authId, aUser(authId), 2L, aUser(2L)))
      .build();

    when(listRepository.getAllByAdminIdOrUsersIn(1, singletonList(1L))).thenReturn(List.of(loadedEntity));

    ListDto list = sut.getLists().get(0);

    assertEquals(loadedEntity.getId(), list.getId());
    assertThat(list.getImps()).hasSize(2);
    assertThat(list.getImps()).containsOnlyOnce(imp1);
    assertThat(list.getImps()).containsOnlyOnce(imp2);

    verify(listRepository).getAllByAdminIdOrUsersIn(1, singletonList(1L));
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_delete_list() {
    ListEntity listEntity = ListEntity.builder().id(1).adminId(55).build();
    when(listRepository.findById(1L)).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(55).build()));

    sut.delete(1L);

    verify(userDetailService).authenticatedUser();
    verify(listRepository).delete(listEntity);
    verify(listRepository).findById(1L);
  }

  @Test
  public void should_not_delete_list_when_not_admin() {
    ListEntity listEntity = aList();
    when(listRepository.findById(1L)).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(66).build()));

    Exception exception = assertThrows(AccessDeniedException.class, () -> sut.delete(1L));
    assertEquals("Unauthorized", exception.getMessage());

    verify(userDetailService).authenticatedUser();
    verify(listRepository).findById(1L);
  }

  @Test
  public void should_update_list() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());
    ListUpdateDto dto = new ListUpdateDto();
    dto.name = "a new name";

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    ListDto updated = sut.update(listEntity.getId(), dto);

    assertEquals(listEntity.getId(), updated.getId());
    assertEquals(dto.name, updated.getName());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService, times(2)).authenticatedUser();
    verify(listRepository).save(listEntity);
  }

  @Test
  public void should_not_update_null_values_list() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());
    ListUpdateDto dto = new ListUpdateDto();

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    ListDto updated = sut.update(listEntity.getId(), dto);

    assertEquals(listEntity.getId(), updated.getId());
    assertEquals(1, updated.getStatus());
    assertEquals("Lorem", updated.getName());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService, times(2)).authenticatedUser();
    verify(listRepository).save(listEntity);
  }

  @Test
  public void should_not_update_when_unauthorized() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(66);
    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));

    Exception exeption = assertThrows(AccessDeniedException.class, () -> sut.update(1L, new ListUpdateDto()));

    assertEquals("Unauthorized", exeption.getMessage());
    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_set_PICKING() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    listEntity.setStatus(INVITING);
    sut.setToPicking(listEntity.getId());

    assertEquals(PICKING, listEntity.getStatus());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService).authenticatedUser();
    verify(listImpService).pickImps(listEntity.getId());
    verify(listRepository).save(listEntity);
  }

  @Test
  public void should_throw_when_setToPicking_on_resolving() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));

    listEntity.setStatus(RESOLVING);
    Exception exception = assertThrows(Exception.class, () -> sut.setToPicking(listEntity.getId()));

    assertEquals("Invalid Status", exception.getMessage());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_set_RESOLVING() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));
    when(listRepository.save(listEntity)).thenReturn(listEntity);

    listEntity.setStatus(PICKING);
    sut.setToResolving(listEntity.getId());

    assertEquals(RESOLVING, listEntity.getStatus());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService).authenticatedUser();
    verify(listRepository).save(listEntity);
  }

  @Test
  public void should_throw_when_setToResolving_on_inviting() {
    ListEntity listEntity = aList();
    UserEntity user = aUser(listEntity.getAdminId());

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(user));

    listEntity.setStatus(INVITING);
    Exception exception = assertThrows(Exception.class, () -> sut.setToResolving(listEntity.getId()));

    assertEquals("Invalid Status", exception.getMessage());

    verify(listRepository).findById(listEntity.getId());
    verify(userDetailService).authenticatedUser();
  }

  @Test
  public void should_add_mayNot() {
    ListEntity entity = new ListEntity();
    entity.setAdminId(8L);
    UserEntity giver = UserEntity.builder().id(1L).build();
    UserEntity reciver = UserEntity.builder().id(2L).build();

    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(8).build()));
    when(listRepository.findById(88L)).thenReturn(Optional.of(entity));
    when(listRepository.save(entity)).thenReturn(entity);
    when(userRepository.findById(1L)).thenReturn(Optional.of(giver));
    when(userRepository.findById(2L)).thenReturn(Optional.of(reciver));

    sut.mayNot(88L, 1L, 2L);

    assertEquals(1L, entity.getMayNot().get(0).getGiver().getId());
    assertEquals(2L, entity.getMayNot().get(0).getReciver().getId());
    assertTrue(entity.getMayNot().get(0).isMayNot());

    verify(listRepository).findById(88L);
    verify(userRepository).findById(1L);
    verify(userRepository).findById(2L);
    verify(impRepository).save(any(ImpEntity.class));
    verify(listRepository).save(entity);
  }

  @Test
  public void should_not_add_mayNot_when_user_not_found() {
    ListEntity entity = new ListEntity();
    entity.setAdminId(8L);

    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(UserEntity.builder().id(8).build()));
    when(listRepository.findById(88L)).thenReturn(Optional.of(entity));
    when(userRepository.findById(1L)).thenReturn(Optional.empty());

    Exception exception = assertThrows(Exception.class, () -> sut.mayNot(88L, 1L, 2L));
    assertEquals("User 1 not found", exception.getMessage());

    verify(listRepository).findById(88L);
    verify(userRepository).findById(1L);
  }

  @Test
  public void should_remove_mayNot() {
    ImpEntity imp = ImpEntity.builder().id(90).build();

    UserEntity authenticatedUser = UserEntity.builder().id(55).build();
    ListEntity listEntity = aList();
    listEntity.setImps(List.of(imp));

    when(impRepository.findById(imp.getId())).thenReturn(Optional.of(imp));
    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));

    sut.removeMayNot(listEntity.getId(), 90L);

    assertEquals(0, listEntity.getMayNot().size());

    verify(userDetailService).authenticatedUser();
    verify(listRepository).findById(listEntity.getId());
    verify(impRepository).findById(imp.getId());
    verify(impRepository).delete(imp);
    verify(listRepository).save(listEntity);
  }

  @Test
  public void should_not_remove_mayNot_when_impId_not_in_list() {
    UserEntity authenticatedUser = UserEntity.builder().id(55).build();
    ListEntity listEntity = aList();
    listEntity.setImps(List.of());

    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));

    Exception exception = assertThrows(Exception.class, () -> sut.removeMayNot(listEntity.getId(), 90L));
    assertEquals("Imp not in List", exception.getMessage());

    verify(userDetailService).authenticatedUser();
    verify(listRepository).findById(listEntity.getId());
  }

  @Test
  public void should_not_remove_mayNot_when_imp_not_found() {
    ImpEntity imp = ImpEntity.builder().id(90).build();

    UserEntity authenticatedUser = UserEntity.builder().id(55).build();
    ListEntity listEntity = aList();
    listEntity.setImps(new ArrayList<>(List.of(imp)));

    when(impRepository.findById(imp.getId())).thenReturn(Optional.empty());
    when(listRepository.findById(listEntity.getId())).thenReturn(Optional.of(listEntity));
    when(userDetailService.authenticatedUser()).thenReturn(Optional.of(authenticatedUser));

    Exception exception = assertThrows(Exception.class, () -> sut.removeMayNot(listEntity.getId(), 90L));
    assertEquals("Imp not found", exception.getMessage());

    verify(userDetailService).authenticatedUser();
    verify(listRepository).findById(listEntity.getId());
    verify(impRepository).findById(imp.getId());
  }

  private ListEntity aList() {
    return ListEntity.builder().id(1).name("Lorem").status(1).adminId(55).build();
  }

}
