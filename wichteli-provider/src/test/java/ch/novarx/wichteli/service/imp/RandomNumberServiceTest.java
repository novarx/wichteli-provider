package ch.novarx.wichteli.service.imp;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.assertj.core.api.Assertions.assertThat;

class RandomNumberServiceTest {
  RandomNumberService sut = new RandomNumberService();

  @Test
  // I don't liked to use Powermockito
  public void should_get_random_numbers() {
    Set<Integer> ints = new HashSet<>();
    for (int i = 0; i < 200; i++) {
      ints.add(sut.randomIntTo(20));
    }

    assertThat(ints.size()).isGreaterThan(0);
    ints.forEach(i -> assertThat(i).isLessThanOrEqualTo(20));
  }
}
