package ch.novarx.wichteli.service;

import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.entity.UserRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import java.util.Optional;

import static org.junit.Assert.assertNotNull;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

  @Mock
  UserRepository userRepository;

  @Mock
  BCryptPasswordEncoder bcryptEncoder;

  @InjectMocks
  UserService sut;

  @AfterEach
  void teardown() {
    verifyNoMoreInteractions(userRepository);
  }

  @Test
  public void make_sure_everything_is_wired_up_correctly() {
    assertNotNull(sut);
    assertNotNull(userRepository);
  }

  @Test
  public void should_load_user_by_username() {
    UserEntity userEntity = new UserEntity();
    userEntity.setUsername("lorem");
    userEntity.setEmail("lorem");
    when(userRepository.findByEmail(userEntity.getUsername())).thenReturn(userEntity);

    UserDto user = sut.loadUserByUsername(userEntity.getUsername());

    assertEquals(userEntity.getEmail(), user.getEmail());
    assertEquals(userEntity.getUsername(), user.getUsername());
    verify(userRepository).findByEmail(userEntity.getUsername());
  }

  @Test
  public void should_throw_when_user_not_found_by_username() {
    when(userRepository.findByEmail(anyString())).thenReturn(null);

    UsernameNotFoundException exception = assertThrows(UsernameNotFoundException.class, () -> sut.loadUserByUsername("NOT_A_USER"));

    assertEquals("User not found with username: NOT_A_USER", exception.getMessage());
    verify(userRepository).findByEmail(anyString());
  }

  @Test
  public void should_update_user() {
    long id = 1L;
    UserDto userDto = UserDto.builder()
            .username("New Name")
            .password("new pass")
            .email("new@example.org")
            .build();
    UserEntity user = UserEntity.builder()
            .username("Old Name")
            .email("info@example.org")
            .password("old pass")
            .build();
    when(userRepository.findById(id)).thenReturn(Optional.of(user));
    when(bcryptEncoder.encode(userDto.getPassword())).thenReturn("HASEDdtoPASS");

    UserDto updated = sut.update(id, userDto);

    assertEquals(userDto.getUsername(), updated.getUsername());
    assertEquals("HASEDdtoPASS", user.getPassword());
    assertNotEquals(userDto.getEmail(), updated.getEmail());

    verify(bcryptEncoder).encode(userDto.getPassword());
    verify(userRepository).findById(id);
    verify(userRepository).save(user);
  }

}
