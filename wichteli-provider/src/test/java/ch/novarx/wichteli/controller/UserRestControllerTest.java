package ch.novarx.wichteli.controller;

import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.service.UserService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class UserRestControllerTest extends BaseControllerTest {

  @MockBean
  UserService userService;

  @Test
  public void should_not_get_a_user_when_unauthenticated() throws Exception {
    mockMvc.perform(get("/user"))
            .andExpect(status().isUnauthorized());
  }

  @Test
  public void should_get_a_user_when_authenticated() throws Exception {
    when(userService.loadUserByUsername(anyString())).thenReturn(UserDto.builder().build());
    mockMvc.perform(get("/user").header(AUTH_HEADER_NAME, BEARER_TOKEN))
            .andExpect(status().isOk());
  }

  @Test
  public void should_update_user() throws Exception {
    UserDto updateDto = UserDto.builder()
            .id(1)
            .username("New Name")
            .password("new pass")
            .email("new@example.org")
            .build();

    when(userService.update(eq(1L), any(UserDto.class))).thenReturn(updateDto);
    mockMvc.perform(put("/user/1").header(AUTH_HEADER_NAME, BEARER_TOKEN)
            .contentType(APPLICATION_JSON)
            .content(objectMapper.writeValueAsString(updateDto)))
            .andExpect(status().isOk())
            .andExpect(content().contentType(APPLICATION_JSON))
            .andExpect(jsonPath("$.id", is(1)))
            .andExpect(jsonPath("$.username", is(updateDto.getUsername())))
            .andExpect(jsonPath("$.email", is(updateDto.getEmail())));
    verify(userService).update(eq(1L), any(UserDto.class));
  }

}
