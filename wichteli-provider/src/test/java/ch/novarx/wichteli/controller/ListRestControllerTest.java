package ch.novarx.wichteli.controller;

import ch.novarx.wichteli.controller.list.dto.ListDto;
import ch.novarx.wichteli.controller.list.dto.ListUpdateDto;
import ch.novarx.wichteli.entity.ImpEntity;
import ch.novarx.wichteli.entity.ListEntity;
import ch.novarx.wichteli.entity.ListRepository;
import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.service.ListService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.test.web.servlet.ResultActions;

import java.util.Collections;

import static ch.novarx.wichteli.types.ListStatusEnum.INVITING;
import static ch.novarx.wichteli.types.ListStatusEnum.PICKING;
import static java.util.Collections.singletonList;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

class ListRestControllerTest extends BaseControllerTest {

  @MockBean
  private ListRepository repository;

  @MockBean
  private ListService listService;

  @AfterEach
  void teardown() {
    verifyNoMoreInteractions(listService);
  }

  @Test
  public void should_get_lists() throws Exception {
    ListEntity listEntity = aListEntity();

    when(listService.getLists()).thenReturn(singletonList(ListDto.of(listEntity)));

    mockMvc.perform(get("/list").header(AUTH_HEADER_NAME, BEARER_TOKEN)
      .contentType(APPLICATION_JSON))
      .andExpect(status().isOk())
      .andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$[0].id", is((int) listEntity.getId())))
      .andExpect(jsonPath("$[0].name", is(listEntity.getName())))
      .andExpect(jsonPath("$[0].adminId", is((int) listEntity.getAdminId())))
      .andExpect(jsonPath("$[0].status", is(listEntity.getStatus())))
      .andExpect(jsonPath("$[0].users.55.id", is(55)));

    verify(listService, times(1)).getLists();
  }

  @Test
  public void should_get_one_specific_list() throws Exception {
    ListEntity listEntity = aListEntity();

    when(listService.getList(listEntity.getId())).thenReturn(ListDto.of(listEntity));

    ResultActions actions = mockMvc.perform(get("/list/" + listEntity.getId()).header(AUTH_HEADER_NAME, BEARER_TOKEN)
      .contentType(APPLICATION_JSON))
      .andExpect(status().isOk());

    assertCorrectResponse(actions, listEntity);
    verify(listService, times(1)).getList(listEntity.getId());
  }

  @Test
  public void should_create_list() throws Exception {
    ListEntity list = aListEntity();
    when(listService.freshList(list.getName())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(post("/list").header(AUTH_HEADER_NAME, BEARER_TOKEN)
      .content("{\"name\": \"" + list.getName() + "\"}")
      .contentType(APPLICATION_JSON))
      .andExpect(status().isCreated());

    assertCorrectResponse(actions, list);
    verify(listService).freshList(list.getName());
  }

  @Test
  public void should_join_user_to_list() throws Exception {
    ListEntity list = aListEntity();

    when(listService.join(list.getJoinCode())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(put("/list/join/" + list.getJoinCode()).header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).join(list.getJoinCode());
  }

  @Test
  public void should_leave_user_from_list() throws Exception {
    ListEntity list = aListEntity();
    when(listService.leave(1, 55)).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(delete("/list/" + list.getId() + "/user/" + 55)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).leave(1, 55);
  }

  @Test
  public void should_not_leave_user_from_list_when_unauthorized() throws Exception {
    ListEntity list = aListEntity();
    when(listService.leave(1, 55)).thenThrow(new AccessDeniedException("Unauthorized"));

    mockMvc.perform(delete("/list/" + list.getId() + "/user/" + 55)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isUnauthorized());

    verify(listService).leave(1, 55);
  }

  @Test
  public void should_delete_list() throws Exception {
    ListEntity list = aListEntity();

    mockMvc.perform(delete("/list/" + list.getId())
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk());

    verify(listService).delete(1);
  }

  @Test
  public void should_not_delete_list_when_unauthorized() throws Exception {
    doThrow(AccessDeniedException.class).when(listService).delete(1);
    mockMvc.perform(delete("/list/1")
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isUnauthorized());
    verify(listService).delete(1);
  }

  @Test
  public void should_set_list_to_picking() throws Exception {
    ListEntity list = aListEntity();
    list.setStatus(INVITING);

    when(listService.getList(list.getId())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(put("/list/" + list.getId() + "/status/picking")
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).setToPicking(list.getId());
    verify(listService).getList(list.getId());
  }

  @Test
  public void should_set_list_to_resolving() throws Exception {
    ListEntity list = aListEntity();
    list.setStatus(PICKING);

    when(listService.getList(list.getId())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(put("/list/" + list.getId() + "/status/resolving")
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).setToResolving(list.getId());
    verify(listService).getList(list.getId());
  }

  @Test
  public void should_update_list() throws Exception {
    ListEntity list = aListEntity();

    when(listService.update(eq(list.getId()), any(ListUpdateDto.class))).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(put("/list/" + list.getId())
      .content("{\"name\": \"a name\"}")
      .contentType(APPLICATION_JSON)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    ArgumentCaptor<ListUpdateDto> captor = ArgumentCaptor.forClass(ListUpdateDto.class);
    verify(listService).update(eq(1L), captor.capture());
    assertEquals("a name", captor.getValue().name);
  }

  @Test
  public void should_not_update_list_when_unauthorized() throws Exception {
    doThrow(AccessDeniedException.class).when(listService).update(eq(1L), any(ListUpdateDto.class));
    mockMvc.perform(put("/list/1")
      .content("{\"name\": \"1\"}")
      .contentType(APPLICATION_JSON)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isUnauthorized());
    verify(listService).update(eq(1L), any(ListUpdateDto.class));
  }

  @Test
  public void should_add_maynot() throws Exception {
    ListEntity list = aListEntity();
    when(listService.getList(list.getId())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(post("/list/" + list.getId() + "/maynot")
      .content("{\"giverId\": 1, \"reciverId\": 2}")
      .contentType(APPLICATION_JSON)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).mayNot(list.getId(), 1L, 2L);
    verify(listService).getList(list.getId());
  }

  @Test
  public void should_return_error_on_maynot_when_wrong_dto() throws Exception {
    mockMvc.perform(post("/list/1/maynot")
      .content("{\"giver\": 1, \"reciverId\": 0}")
      .contentType(APPLICATION_JSON)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isBadRequest());
  }

  @Test
  public void should_delete_maynot() throws Exception {
    ListEntity list = aListEntity();
    when(listService.getList(list.getId())).thenReturn(ListDto.of(list));

    ResultActions actions = mockMvc.perform(delete("/list/" + list.getId() + "/maynot/2")
      .contentType(APPLICATION_JSON)
      .header(AUTH_HEADER_NAME, BEARER_TOKEN))
      .andExpect(status().isOk())
      .andExpect(content().contentType(APPLICATION_JSON));

    assertCorrectResponse(actions, list);
    verify(listService).removeMayNot(list.getId(), 2L);
    verify(listService).getList(list.getId());
  }


  private ListEntity aListEntity() {
    return ListEntity.builder()
      .id(1)
      .name("A List")
      .adminId(88)
      .joinCode("aCode")
      .status(INVITING)
      .imps(singletonList(ImpEntity.builder().id(67).mayNot(true).build()))
      .users(Collections.singletonMap(55L, UserEntity.builder().id(55).build()))
      .build();
  }

  private void assertCorrectResponse(ResultActions actions, ListEntity list) throws Exception {
    int firstUser = list.getUsers().keySet().iterator().next().intValue();
    actions.andExpect(content().contentType(MediaType.APPLICATION_JSON))
      .andExpect(jsonPath("$.id", is((int) list.getId())))
      .andExpect(jsonPath("$.name", is(list.getName())))
      .andExpect(jsonPath("$.adminId", is((int) list.getAdminId())))
      .andExpect(jsonPath("$.status", is(list.getStatus())))
      .andExpect(jsonPath("$.users." + firstUser + ".id", is(firstUser)))
      .andExpect(jsonPath("$.mayNot[0].id", is((int) list.getMayNot().get(0).getId())));
  }

}
