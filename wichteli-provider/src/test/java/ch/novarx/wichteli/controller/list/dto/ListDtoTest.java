package ch.novarx.wichteli.controller.list.dto;

import ch.novarx.wichteli.entity.ImpEntity;
import ch.novarx.wichteli.entity.ListEntity;
import ch.novarx.wichteli.entity.UserEntity;
import org.junit.jupiter.api.Test;

import java.util.List;

import static ch.novarx.wichteli.types.ListStatusEnum.INVITING;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

class ListDtoTest {
  @Test
  public void should_convert_listEntity_to_dto() {
    UserEntity admin = UserEntity.builder().id(11).build();
    ImpEntity mayNot = ImpEntity.builder().id(1).mayNot(true).build();
    ImpEntity imp = ImpEntity.builder().id(2).mayNot(false).build();

    ListEntity listEntity = ListEntity.builder()
      .id(11)
      .name("Lorem")
      .adminId(admin.getId())
      .imps(List.of(mayNot, imp))
      .status(INVITING)
      .users(singletonMap(admin.getId(), admin))
      .joinCode("aCode")
      .build();

    ListDto converted = ListDto.of(listEntity);

    assertEquals(listEntity.getId(), converted.getId());
    assertEquals(listEntity.getName(), converted.getName());
    assertEquals(listEntity.getAdminId(), converted.getAdminId());
    assertEquals(listEntity.getStatus(), converted.getStatus());
    assertEquals(listEntity.getJoinCode(), converted.getJoinCode());
    assertEquals(listEntity.getMayNot(), converted.getMayNot());
    assertThat(converted.getMayNot())
      .containsAll(listEntity.getImps().stream().filter(ImpEntity::isMayNot).collect(toList()));
    assertThat(converted.getImps())
      .containsAll(listEntity.getImps().stream().filter(i -> !i.isMayNot()).collect(toList()));
    assertEquals(listEntity.getUsers(), converted.getUsers());
  }
}
