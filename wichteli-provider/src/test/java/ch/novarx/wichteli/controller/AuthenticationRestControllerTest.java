package ch.novarx.wichteli.controller;

import ch.novarx.wichteli.controller.security.dto.JwtRequest;
import ch.novarx.wichteli.controller.user.UserDto;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

class AuthenticationRestControllerTest extends BaseControllerTest {

  @MockBean
  private AuthenticationManager authenticationManager;

  private UserDto getUser() {
    return UserDto.builder()
            .username("newUser")
            .email("newUser@example.com")
            .password("ipsum")
            .build();
  }

  @Test
  public void a_user_should_get_registered() throws Exception {
    mockMvc.perform(post("/register")
            .content(objectMapper.writeValueAsString(getUser()))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isCreated());
  }

  @Test
  public void a_user_should_get_authenticated() throws Exception {
    when(jwtTokenUtil.generateToken(any())).thenReturn("Bearer xyz");
    when(authenticationManager.authenticate(any())).thenAnswer(i -> i.getArgument(0));
    mockMvc.perform(post("/authenticate")
            .content(objectMapper.writeValueAsString(getUser()))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isOk())
            .andExpect(jsonPath("$.token", is("Bearer xyz")));
  }

  @Test
  public void a_invalid_user_should_not_get_authenticated() throws Exception {
    when(authenticationManager.authenticate(any())).thenThrow(new BadCredentialsException(""));
    JwtRequest jwtRequest = new JwtRequest();
    jwtRequest.setEmail("merol");
    jwtRequest.setPassword("muspi");

    mockMvc.perform(post("/authenticate")
            .content(objectMapper.writeValueAsString(jwtRequest))
            .contentType(MediaType.APPLICATION_JSON))
            .andExpect(status().isUnauthorized());
  }

}
