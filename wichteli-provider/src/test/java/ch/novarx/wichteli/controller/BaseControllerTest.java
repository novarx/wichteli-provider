package ch.novarx.wichteli.controller;

import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.security.JwtTokenUtil;
import ch.novarx.wichteli.security.JwtUserDetailService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.Optional;

import static org.mockito.Mockito.when;

@RunWith(SpringRunner.class)
@ContextConfiguration
@SpringBootTest
@AutoConfigureMockMvc
public abstract class BaseControllerTest {
  protected static final String AUTH_HEADER_NAME = "Authorization";
  protected static final String TOKEN = "eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiJsb3JlbSIsImV4cCI6MTU5MTYxMDQ1NiwiaWF0IjoxNTkxNTkyNDU2fQ.rn90fJdw9kYAChrD9yIk7yS9tmMX7qE8VQIi1MkXTASIoRc-KV63GYKLTxib5cqeMxlMvMcbMadY-jPHKCNyzg";
  protected static final String BEARER_TOKEN = "Bearer " + TOKEN;

  @MockBean
  private JwtUserDetailService jwtUserDetailService;

  @MockBean
  protected JwtTokenUtil jwtTokenUtil;

  @Autowired
  MockMvc mockMvc;

  protected ObjectMapper objectMapper = new ObjectMapper();

  @BeforeEach
  private void setup() {
    UserEntity user = UserEntity.builder().id(88L).username("lorem").email("info@example.org").build();
    UserDetails userDetail = new User(user.getUsername(), "ipsum", new ArrayList<>());
    when(jwtUserDetailService.loadUserByUsername("lorem")).thenReturn(userDetail);
    when(jwtUserDetailService.authenticatedUser()).thenReturn(Optional.of(user));

    when(jwtTokenUtil.getUsernameFromToken(TOKEN)).thenReturn("lorem");
    when(jwtTokenUtil.validateToken(TOKEN, userDetail)).thenReturn(true);
  }

}
