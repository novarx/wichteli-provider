CREATE TABLE IF NOT EXISTS `lists` (

    `id` int NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` varchar(255) NOT NULL,
    `admin_id` int NOT NULL,
    `status` int NOT NULL

);
