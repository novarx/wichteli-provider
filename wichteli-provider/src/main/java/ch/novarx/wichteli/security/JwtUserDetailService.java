package ch.novarx.wichteli.security;

import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.entity.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
public class JwtUserDetailService implements UserDetailsService {

  @Autowired
  UserRepository userRepository;

  @Autowired
  private PasswordEncoder bcryptEncoder;

  @Override
  public UserDetails loadUserByUsername(String email) {
    UserEntity user = userRepository.findByEmail(email);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with email: " + email);
    }
    return new User(user.getEmail(), user.getPassword(), new ArrayList<>());
  }

  public UserEntity save(UserDto user) {
    if (userRepository.findByEmail(user.getUsername()) != null) {
      throw new UsernameNotFoundException("Username already taken: " + user.getUsername());
    }
    String hashedPassword = bcryptEncoder.encode(user.getPassword());
    UserEntity newUser = UserEntity.builder()
            .username(user.getUsername())
            .email(user.getEmail())
            .password(hashedPassword)
            .build();
    return userRepository.save(newUser);
  }

  public Optional<UserEntity> authenticatedUser() {
    if (SecurityContextHolder.getContext().getAuthentication() != null) {
      String username = SecurityContextHolder.getContext().getAuthentication().getName();
      return Optional.of(userRepository.findByEmail(username));
    }
    return Optional.empty();
  }
}
