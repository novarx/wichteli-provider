package ch.novarx.wichteli.security;

import org.springframework.stereotype.Service;

@Service
public class TimeProvider {
  public long currentTimeMillis() {
    return System.currentTimeMillis();
  }
}
