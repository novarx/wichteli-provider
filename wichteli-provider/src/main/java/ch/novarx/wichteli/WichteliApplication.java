package ch.novarx.wichteli;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WichteliApplication {

  public static void main(String[] args) {
    SpringApplication.run(WichteliApplication.class, args);
  }

}
