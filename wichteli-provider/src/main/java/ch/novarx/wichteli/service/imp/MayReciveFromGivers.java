package ch.novarx.wichteli.service.imp;

import ch.novarx.wichteli.entity.ListEntity;
import ch.novarx.wichteli.entity.UserEntity;

import java.util.*;
import java.util.stream.Collectors;

import static java.util.Comparator.comparingInt;
import static java.util.stream.Collectors.toList;

class MayReciveFromGivers {
  long reciver;
  List<Long> givers;

  public MayReciveFromGivers(long reciver, List<Long> allUserIds) {
    this.reciver = reciver;
    this.givers = new ArrayList<>();
    this.givers.addAll(allUserIds);
    rm(reciver);
  }

  void rm(long giver) {
    givers.remove(giver);
  }

  /**
   * @param list From what list to extract the tuples
   * @return a Queue containing reciver, giver tuples, ordered by the Size of possible Givers.
   */
  static PriorityQueue<MayReciveFromGivers> mayReciveFromGiversQueue(ListEntity list) {
    List<Long> allUserIds = list.getUsers().values().stream().map(UserEntity::getId).collect(toList());
    Map<Long, MayReciveFromGivers> reciversMappedToAllPossibleGivers =
      allUserIds.stream().collect(Collectors.toMap(
        id -> id,
        id -> new MayReciveFromGivers(id, allUserIds)
      ));

    removeMayNots(list, reciversMappedToAllPossibleGivers);

    return priorityQueueFor(reciversMappedToAllPossibleGivers);
  }

  private static PriorityQueue<MayReciveFromGivers> priorityQueueFor(Map<Long, MayReciveFromGivers> reciversMappedToAllPossibleGivers) {
    PriorityQueue<MayReciveFromGivers> giversForReciverSorted = new PriorityQueue<>(orderBySize());
    reciversMappedToAllPossibleGivers.forEach((i, mayRecive) -> giversForReciverSorted.add(mayRecive));
    return giversForReciverSorted;
  }

  private static void removeMayNots(ListEntity list, Map<Long, MayReciveFromGivers> reciversMappedToAllPossibleGivers) {
    list.getMayNot().forEach(mayNot -> {
      long reciver = mayNot.getReciver().getId();
      long giver = mayNot.getGiver().getId();
      reciversMappedToAllPossibleGivers.get(reciver).rm(giver);
    });
  }

  private static Comparator<MayReciveFromGivers> orderBySize() {
    return comparingInt(m -> m.givers.size());
  }
}
