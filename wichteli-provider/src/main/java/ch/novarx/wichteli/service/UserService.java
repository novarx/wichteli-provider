package ch.novarx.wichteli.service;

import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.entity.UserEntity;
import ch.novarx.wichteli.entity.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.validation.constraints.NotNull;

@Service
public class UserService {

  @Autowired
  PasswordEncoder bcryptEncoder;

  @Autowired
  UserRepository userRepository;

  public UserDto loadUserByUsername(String username) {
    UserEntity user = userRepository.findByEmail(username);
    if (user == null) {
      throw new UsernameNotFoundException("User not found with username: " + username);
    }
    return UserDto.of(user);
  }

  public UserDto update(long userId, UserDto userDto) {
    UserEntity user = getUserOrThrow(userId);
    if (userDto.hasUsername()) {
      user.setUsername(userDto.getUsername());
    }
    if (userDto.hasPassword()) {
      String hashedPassword = bcryptEncoder.encode(userDto.getPassword());
      user.setPassword(hashedPassword);
    }
    userRepository.save(user);
    return UserDto.of(user);
  }

  @NotNull
  private UserEntity getUserOrThrow(long userId) {
    return userRepository.findById(userId).orElseThrow(
            () -> new UsernameNotFoundException("User not found with id: " + userId));
  }
}
