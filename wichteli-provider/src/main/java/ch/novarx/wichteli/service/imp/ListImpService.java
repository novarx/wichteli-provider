package ch.novarx.wichteli.service.imp;

import ch.novarx.wichteli.entity.ImpEntity;
import ch.novarx.wichteli.entity.ImpRepository;
import ch.novarx.wichteli.entity.ListEntity;
import ch.novarx.wichteli.entity.ListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

import static ch.novarx.wichteli.service.imp.MayReciveFromGivers.mayReciveFromGiversQueue;

@Service
public class ListImpService {
  @Autowired
  private ListRepository listRepository;

  @Autowired
  private ImpRepository impRepository;

  @Autowired
  private RandomNumberService randomNumberService;

  public void pickImps(long listId) {
    ListEntity list = listRepository.findById(listId).orElseThrow();

    Map<Long, Long> giversAndRecivers = getGiversAndRecivers(list);
    giversAndRecivers.forEach((giver, reciver) -> {
      ImpEntity imp = getImpFor(list, giver, reciver);
      impRepository.save(imp);
      list.addImp(imp);
    });

    listRepository.save(list);
  }

  private ImpEntity getImpFor(ListEntity list, Long giver, Long reciver) {
    return ImpEntity.builder().giver(list.getUsers().get(giver)).reciver(list.getUsers().get(reciver)).build();
  }

  private Map<Long, Long> getGiversAndRecivers(ListEntity list) {
    PriorityQueue<MayReciveFromGivers> mayReciveFrom = mayReciveFromGiversQueue(list);
    Map<Long, Long> imps = new HashMap<>();
    MayReciveFromGivers may;

    while ((may = mayReciveFrom.poll()) != null) {
      long giver = getRandomGiverOrThrow(may);
      imps.put(giver, may.reciver);
      mayReciveFrom.forEach(m -> m.rm(giver));
    }

    return imps;
  }

  private long getRandomGiverOrThrow(MayReciveFromGivers may) {
    if (may.givers.isEmpty()) {
      throw new RuntimeException("Not enough imps for Everybody!");
    }
    int randomIndex = randomNumberService.randomIntTo(may.givers.size() - 1);
    return may.givers.get(randomIndex);
  }

}
