package ch.novarx.wichteli.service.imp;

import org.springframework.stereotype.Service;

@Service
public class RandomNumberService {
  public int randomIntTo(int max) {
    return (int) Math.floor(Math.random() * (max));
  }
}
