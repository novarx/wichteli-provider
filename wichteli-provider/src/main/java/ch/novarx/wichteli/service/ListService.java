package ch.novarx.wichteli.service;

import ch.novarx.wichteli.controller.list.dto.ListDto;
import ch.novarx.wichteli.controller.list.dto.ListUpdateDto;
import ch.novarx.wichteli.entity.*;
import ch.novarx.wichteli.security.JwtUserDetailService;
import ch.novarx.wichteli.service.imp.ListImpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.stereotype.Service;

import java.security.SecureRandom;
import java.util.List;

import static ch.novarx.wichteli.controller.list.dto.ListDto.of;
import static ch.novarx.wichteli.types.ListStatusEnum.*;
import static java.util.Collections.*;
import static java.util.stream.Collectors.toList;

@Service
public class ListService {
  static final String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
  static SecureRandom rnd = new SecureRandom();

  @Autowired
  private ListRepository listRepository;

  @Autowired
  private JwtUserDetailService userDetailService;

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ImpRepository impRepository;

  @Autowired
  private ListImpService listImpService;

  public ListDto freshList(String name) {
    UserEntity admin = userDetailService.authenticatedUser().orElseThrow();

    ListEntity list = ListEntity.builder()
      .name(name)
      .adminId(admin.getId())
      .joinCode(randomString())
      .users(singletonMap(admin.getId(), admin))
      .build();
    return of(listRepository.save(list));
  }

  public ListDto getList(long listId) {
    long userId = userDetailService.authenticatedUser().orElseThrow().getId();
    ListEntity list = listRepository.findById(listId)
      .orElseThrow(() -> new RuntimeException("List not found"));
    throwWhenNotInList(list);
    return convertList(list, userId);
  }

  public List<ListDto> getLists() {
    long userId = userDetailService.authenticatedUser().orElseThrow().getId();
    return listRepository.getAllByAdminIdOrUsersIn(userId, singletonList(userId))
      .stream().map(l -> convertList(l, userId)).collect(toList());
  }

  public ListDto update(long listId, ListUpdateDto dto) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);
    list.setName(dto.name != null ? dto.name : list.getName());
    return convertList(listRepository.save(list));
  }

  public ListDto leave(long listId, long userId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotInList(list);

    list.removeUser(userId);
    return convertList(listRepository.save(list));
  }

  public void removeMayNot(long listId, long impId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);

    if (list.getImps().stream().noneMatch(l -> l.getId() == impId)) {
      throw new RuntimeException("Imp not in List");
    }
    ImpEntity imp = impRepository.findById(impId).orElseThrow(() -> new RuntimeException("Imp not found"));

    list.removeImp(impId);
    listRepository.save(list);

    impRepository.delete(imp);
  }

  public ListDto join(String joinCode) {
    ListEntity list = listRepository.findByJoinCode(joinCode)
      .orElseThrow(() -> new RuntimeException("joinCode: " + joinCode + " not found"));
    UserEntity user = userDetailService.authenticatedUser().orElseThrow();

    list.addUser(user);
    return of(listRepository.save(list));
  }

  public void setToPicking(long listId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);
    if (list.getStatus() == RESOLVING) {
      throw new RuntimeException("Invalid Status");
    }
    list.setStatus(PICKING);

    listImpService.pickImps(list.getId());
    listRepository.save(list);
  }

  public void setToResolving(long listId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);
    if (list.getStatus() == INVITING) {
      throw new RuntimeException("Invalid Status");
    }
    list.setStatus(RESOLVING);

    listRepository.save(list);
  }

  public void mayNot(long listId, long giverId, long reciverId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);

    UserEntity giver = getUserOrThrow(giverId);
    UserEntity reciver = getUserOrThrow(reciverId);

    ImpEntity imp = ImpEntity.builder().giver(giver).reciver(reciver).mayNot(true).build();
    impRepository.save(imp);

    list.addImp(imp);
    listRepository.save(list);
  }

  public void delete(long listId) {
    ListEntity list = readListOrThrow(listId);
    throwWhenNotAdmin(list);
    listRepository.delete(list);
  }

  private ListDto convertList(ListEntity listEntity) {
    long userId = userDetailService.authenticatedUser().orElseThrow().getId();
    return convertList(listEntity, userId);
  }

  private ListDto convertList(ListEntity listEntity, long userId) {
    ListDto converted = ListDto.of(listEntity);
    cleanImpsFor(userId, converted);
    return converted;
  }

  private void cleanImpsFor(long userId, ListDto converted) {
    int status = converted.getStatus();
    if (status == INVITING) {
      converted.setImps(emptyList());
    } else if (status == PICKING) {
      converted.setImps(converted.getImps().stream()
        .filter(i -> i.getGiver().getId() == userId)
        .collect(toList())
      );
    }
  }

  private ListEntity readListOrThrow(long listId) {
    return listRepository.findById(listId).orElseThrow(() -> new RuntimeException("List not found"));
  }

  private UserEntity getUserOrThrow(long userId) {
    return userRepository.findById(userId).orElseThrow(() -> new RuntimeException("User " + userId + " not found"));
  }

  private void throwWhenNotInList(ListEntity list) {
    UserEntity user = userDetailService.authenticatedUser().orElseThrow(() -> new AccessDeniedException("Unauthorized"));
    if (list.getAdminId() != user.getId() && !list.getUsers().containsKey(user.getId())) {
      throw new AccessDeniedException("Unauthorized");
    }
  }

  private void throwWhenNotAdmin(ListEntity list) {
    UserEntity user = userDetailService.authenticatedUser().orElseThrow(() -> new AccessDeniedException("Unauthorized"));
    if (list.getAdminId() != user.getId()) {
      throw new AccessDeniedException("Unauthorized");
    }
  }

  private String randomString() {
    StringBuilder sb = new StringBuilder(25);
    for (int i = 0; i < 25; i++)
      sb.append(AB.charAt(rnd.nextInt(AB.length())));
    return sb.toString();
  }
}
