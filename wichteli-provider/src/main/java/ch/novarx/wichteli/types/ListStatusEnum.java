package ch.novarx.wichteli.types;

import java.util.ArrayList;
import java.util.List;

public final class ListStatusEnum {
  public static final int INVITING = 0;
  public static final int PICKING = 1;
  public static final int RESOLVING = 2;

  // TODO make a abstract class to handle such functionality
  public static List<Integer> values() {
    List<Integer> list = new ArrayList<>();
    list.add(INVITING);
    list.add(PICKING);
    list.add(RESOLVING);
    return list;
  }
}