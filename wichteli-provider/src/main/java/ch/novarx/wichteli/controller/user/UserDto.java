package ch.novarx.wichteli.controller.user;

import ch.novarx.wichteli.entity.UserEntity;
import lombok.*;

@Builder
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {
  private long id;
  private String username;
  private String password;
  private String email;

  public boolean hasPassword() {
    return notEmpty(password);
  }

  public boolean hasUsername() {
    return notEmpty(username);
  }

  private boolean notEmpty(String string) {
    return string != null && !string.equals("");
  }

  public static UserDto of(UserEntity user) {
    return UserDto.builder()
            .id(user.getId())
            .username(user.getUsername())
            .email(user.getEmail())
            .build();
  }
}
