package ch.novarx.wichteli.controller.security;

import ch.novarx.wichteli.controller.security.dto.JwtRequest;
import ch.novarx.wichteli.controller.security.dto.JwtResponse;
import ch.novarx.wichteli.controller.user.UserDto;
import ch.novarx.wichteli.security.JwtTokenUtil;
import ch.novarx.wichteli.security.JwtUserDetailService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@CrossOrigin
@Api(tags = "Authentication")
public class AuthenticationRestController {
  @Autowired
  private AuthenticationManager authenticationManager;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Autowired
  private JwtUserDetailService userDetailsService;

  @PostMapping(value = "/authenticate")
  public ResponseEntity<JwtResponse> createAuthenticationToken(@RequestBody JwtRequest authenticationRequest) throws Exception {
    authenticate(authenticationRequest.getEmail(), authenticationRequest.getPassword());
    final UserDetails userDetails = userDetailsService
      .loadUserByUsername(authenticationRequest.getEmail());
    final String token = jwtTokenUtil.generateToken(userDetails);
    return ResponseEntity.ok(new JwtResponse(token));
  }

  @PostMapping(value = "/register")
  public ResponseEntity<Void> saveUser(@RequestBody UserDto user) {
    userDetailsService.save(user);
    return ResponseEntity.status(HttpStatus.CREATED).build();
  }

  private void authenticate(String email, String password) throws Exception {
    try {
      authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(email, password));
    } catch (Exception e) {
      throw new Exception("UNAUTHORIZED", e);
    }
  }
}
