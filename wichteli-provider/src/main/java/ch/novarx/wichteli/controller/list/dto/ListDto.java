package ch.novarx.wichteli.controller.list.dto;

import ch.novarx.wichteli.entity.ImpEntity;
import ch.novarx.wichteli.entity.ListEntity;
import ch.novarx.wichteli.entity.UserEntity;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Map;

import static com.fasterxml.jackson.annotation.JsonInclude.Include.NON_NULL;
import static java.util.stream.Collectors.toList;

@Getter
@Setter
@Builder
public class ListDto {

  private long id;

  private String name;

  private long adminId;

  private String joinCode;

  private int status;

  private Map<Long, UserEntity> users;

  private List<ImpEntity> mayNot;

  @JsonInclude(NON_NULL)
  private List<ImpEntity> imps;

  public static ListDto of(ListEntity listEntity) {
    return ListDto.builder()
      .id(listEntity.getId())
      .name(listEntity.getName())
      .adminId(listEntity.getAdminId())
      .mayNot(listEntity.getMayNot())
      .imps(listEntity.getImps().stream().filter(i -> !i.isMayNot()).collect(toList()))
      .status(listEntity.getStatus())
      .users(listEntity.getUsers())
      .joinCode(listEntity.getJoinCode())
      .build();
  }
}
