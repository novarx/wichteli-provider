package ch.novarx.wichteli.controller.list.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;

@Getter
@Setter
public class MaynotDto {
  @Min(1)
  private long giverId;
  @Min(1)
  private long reciverId;
}
