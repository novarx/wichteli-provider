package ch.novarx.wichteli.controller.list;

import ch.novarx.wichteli.controller.list.dto.ListDto;
import ch.novarx.wichteli.controller.list.dto.ListNameDto;
import ch.novarx.wichteli.controller.list.dto.ListUpdateDto;
import ch.novarx.wichteli.controller.list.dto.MaynotDto;
import ch.novarx.wichteli.service.ListService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

import static org.springframework.http.HttpStatus.*;

@RestController
@Api(tags = "List")
public class ListRestController {

  @Autowired
  private ListService listService;

  @GetMapping(value = "/list/{listId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ListDto getList(@PathVariable("listId") long listId) {
    return listService.getList(listId);
  }

  @GetMapping("/list")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public List<ListDto> getLists() {
    return listService.getLists();
  }

  @PostMapping(value = "/list")
  @ResponseStatus(CREATED)
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ListDto createList(@RequestBody ListNameDto nameDto) {
    return listService.freshList(nameDto.name);
  }

  @PutMapping(value = "/list/join/{joinCode}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ListDto joinList(@PathVariable("joinCode") String joinCode) {
    return listService.join(joinCode);
  }


  @DeleteMapping(value = "/list/{listId}/user/{userId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> leaveList(@PathVariable long listId, @PathVariable long userId) {
    try {
      return ResponseEntity.ok(listService.leave(listId, userId));
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
  }

  @DeleteMapping(value = "/list/{listId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<Void> deleteList(@PathVariable long listId) {
    try {
      listService.delete(listId);
      return ResponseEntity.status(OK).build();
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
  }

  @PutMapping(value = "/list/{listId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> updateList(@PathVariable long listId, @RequestBody ListUpdateDto listUpdateDto) {
    try {
      ListDto list = listService.update(listId, listUpdateDto);
      return ResponseEntity.ok(list);
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
  }

  @PutMapping(value = "/list/{listId}/status/picking")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> setListToPicking(@PathVariable long listId) {
    try {
      listService.setToPicking(listId);
      return ResponseEntity.ok(listService.getList(listId));
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
  }

  @PutMapping(value = "/list/{listId}/status/resolving")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> setListToResolving(@PathVariable long listId) {
    try {
      listService.setToResolving(listId);
      return ResponseEntity.ok(listService.getList(listId));
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
  }

  @PostMapping(value = "/list/{listId}/maynot")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> maynot(@PathVariable long listId, @RequestBody @Valid MaynotDto listUpdateDto) {
    try {
      listService.mayNot(listId, listUpdateDto.getGiverId(), listUpdateDto.getReciverId());
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
    return ResponseEntity.ok(listService.getList(listId));
  }

  @DeleteMapping(value = "/list/{listId}/maynot/{impId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<ListDto> maynot(@PathVariable long listId, @PathVariable long impId) {
    try {
      listService.removeMayNot(listId, impId);
    } catch (AccessDeniedException e) {
      return ResponseEntity.status(UNAUTHORIZED).build();
    }
    return ResponseEntity.ok(listService.getList(listId));
  }

}
