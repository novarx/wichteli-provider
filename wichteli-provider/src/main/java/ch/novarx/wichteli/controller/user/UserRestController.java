package ch.novarx.wichteli.controller.user;

import ch.novarx.wichteli.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.Authorization;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.ResponseEntity.ok;

@RestController
@Api(tags = "User")
public class UserRestController {

  @Autowired
  UserService userService;

  @GetMapping("/user")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<UserDto> getAuthenticatedUser() {
    Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
    return ok(userService.loadUserByUsername(authentication.getName()));
  }

  @PutMapping("/user/{userId}")
  @ApiOperation(value = "", authorizations = @Authorization("Bearer"))
  public ResponseEntity<UserDto> updateUser(@PathVariable long userId, @RequestBody UserDto userDto) {
    return ok(userService.update(userId, userDto));
  }

}
