package ch.novarx.wichteli.entity;

import ch.novarx.wichteli.types.ListStatusEnum;
import lombok.*;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toList;
import static javax.persistence.FetchType.LAZY;

@Entity(name = "lists")
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ListEntity {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @Column
  @NotEmpty(message = "Name not provided.")
  private String name;

  @Column
  @NotNull(message = "Admin not provided.")
  private long adminId;

  @Column(unique = true)
  private String joinCode;

  @Column
  @Builder.Default
  private int status = ListStatusEnum.INVITING;

  @OneToMany
  @Builder.Default
  private Map<Long, UserEntity> users = new HashMap<>();

  @OneToMany(mappedBy = "list", fetch = LAZY, cascade = CascadeType.ALL)
  @Builder.Default
  private List<ImpEntity> imps = new ArrayList<>();

  public void setStatus(int status) {
    if (ListStatusEnum.values().contains(status)) {
      this.status = status;
    }
  }

  public void addUser(UserEntity user) {
    users.putIfAbsent(user.getId(), user);
  }

  public void addUsers(Map<Long, UserEntity> users) {
    this.users.putAll(users);
  }

  public void removeUser(long userId) {
    if (userId != adminId) {
      users.remove(userId);
    }
  }

  public void addImp(@Valid ImpEntity imp) {
    if (!containsImp(imp)) {
      imp.setList(this);
      imps.add(imp);
    }
  }

  private boolean containsImp(ImpEntity imp) {
    return imps.stream().anyMatch(i ->
            i.getGiver().getId() == imp.getGiver().getId() &&
                    i.getReciver().getId() == imp.getReciver().getId() &&
                    i.isMayNot() == imp.isMayNot()
    );
  }

  public void removeImp(long impId) {
    imps = imps.stream().filter(l -> l.getId() != impId).collect(toList());
  }

  public List<ImpEntity> getMayNot() {
    return imps.stream().filter(ImpEntity::isMayNot).collect(toList());
  }
}
