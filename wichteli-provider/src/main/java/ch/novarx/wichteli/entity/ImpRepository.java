package ch.novarx.wichteli.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface ImpRepository extends JpaRepository<ImpEntity, Long> {
  ImpEntity save(ImpEntity entity);

  Optional<ImpEntity> findById(long id);
}
