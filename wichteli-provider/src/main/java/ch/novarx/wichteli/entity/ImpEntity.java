package ch.novarx.wichteli.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

import static javax.persistence.FetchType.LAZY;

@Entity
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Table(uniqueConstraints = {@UniqueConstraint(columnNames = {"giver_id", "reciver_id", "list_id", "maynot"})})
public class ImpEntity {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private long id;

  @ManyToOne(fetch = LAZY)
  @JsonIgnore
  private ListEntity list;

  @OneToOne
  @NotNull
  private UserEntity giver;

  @OneToOne
  @NotNull
  private UserEntity reciver;

  @Column
  private boolean mayNot;
}
