package ch.novarx.wichteli.entity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ListRepository extends JpaRepository<ListEntity, Long> {
  ListEntity save(ListEntity entity);

  Optional<ListEntity> findById(long id);

  Optional<ListEntity> findByJoinCode(String joinCode);

  List<ListEntity> getAllByAdminIdOrUsersIn(long userId, List<Long> userIds);
}
